package com.hanweb.dto;

import lombok.Data;

/**
 * @author Seven.Yin
 */
@Data
public class IntegralDto {
    private String integralId;
    private String userId;
    private String eventId;
    private String type;
    private int number;
    private String createDate;
}

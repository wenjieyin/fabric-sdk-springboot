package com.hanweb.common;

/**
 * @author Seven.Yin
 */
public class IntegralContractConstant {
    /**
     * fabric配置文件
     */
    public static final String FABRIC_CONFIG_PROPERTIES = "/fabric.config.properties";
    /**
     * 网络配置
     */
    public static final String NETWORK_CONFIG_PATH = "networkConfigPath";
    /**
     * 通道名称
     */
    public static final String CHANNEL_NAME = "channelName";
    /**
     * 智能合约名称
     */
    public static final String CONTRACT_NAME = "contractName";
    /**
     * 证书路径
     */
    public static final String CERTIFICATE_PATH = "certificatePath";
    /**
     * 私钥路径
     */
    public static final String PRIVATE_KEY_PATH = "privateKeyPath";
    /**
     * 签名私钥前缀
     */
    public static final String SIGN_PREFIX = "user1";
    /**
     * 组织1的msg
     */
    public static final String ORG_ONE_MSG = "Org1MSP";
}

package com.hanweb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FabricJavaSdkApplication {

    public static void main(String[] args) {
        SpringApplication.run(FabricJavaSdkApplication.class, args);
    }

}

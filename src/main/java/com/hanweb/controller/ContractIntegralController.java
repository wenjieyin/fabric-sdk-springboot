package com.hanweb.controller;

import com.hanweb.dto.IntegralDto;
import com.hanweb.service.ContractIntegralService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * @author Seven.Yin
 */
@Api(tags = "智能合约调用 API")
@RestController
@RequestMapping("/contract")
public class ContractIntegralController {

    @Resource
    private ContractIntegralService contractIntegralService;

    @ApiOperation("获取所有信息")
    @GetMapping("getAllIntegrals")
    public List<IntegralDto> getAllIntegrals() {
        return contractIntegralService.getAllIntegrals();
    }

    @ApiOperation("单个查询")
    @GetMapping("queryOne/{integralId}")
    public IntegralDto queryOne(@PathVariable("integralId") String integralId) {
        return contractIntegralService.queryOne(integralId);
    }

    @ApiOperation("添加")
    @PostMapping("create")
    public IntegralDto create(@RequestBody IntegralDto integralDto) {
        return contractIntegralService.create(integralDto);
    }

    @ApiOperation("更新")
    @PostMapping("update")
    public IntegralDto update(@RequestBody IntegralDto integralDto) {
        return contractIntegralService.update(integralDto);
    }

    @ApiOperation("删除")
    @GetMapping("deleteIntegral/{integralId}")
    public boolean deleteIntegral(@PathVariable("integralId") String integralId) {
        return contractIntegralService.deleteIntegral(integralId);
    }

    @ApiOperation("根据key查询历史数据")
    @GetMapping("getHistoryDataByKey/{key}")
    public List<Map<String, Object>> getHistoryDataByKey(@PathVariable("key") String key) {
        return contractIntegralService.getHistoryDataByKey(key);
    }

    @ApiOperation("测试-查询ClientIdentity内容")
    @GetMapping("ClientIdentity")
    public String getClientIdentity() {
        return contractIntegralService.getClientIdentity();
    }
}
